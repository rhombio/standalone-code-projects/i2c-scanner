#include <Arduino.h>
#include <Wire.h>
#include "rhio-pinmap.h"

// Tested OK on ESP32, Leonardo, Uno and Duino_Zero
// No response on the Duino_Mega

void setup() {
  Wire.begin();

  RH_UART_DEBUG.begin(9600);
  while (!RH_UART_DEBUG)
    ;  // Leonardo: wait for RH_UART_DEBUG monitor
  RH_UART_DEBUG.println("\nI2C Scanner");
}

void loop() {
  byte error, address;
  int nDevices;

  RH_UART_DEBUG.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++) {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      RH_UART_DEBUG.print("I2C device found at address 0x");
      if (address < 16) RH_UART_DEBUG.print("0");
      RH_UART_DEBUG.print(address, HEX);
      RH_UART_DEBUG.println("  !");

      nDevices++;
    } else if (error == 4) {
      RH_UART_DEBUG.print("Unknown error at address 0x");
      if (address < 16) RH_UART_DEBUG.print("0");
      RH_UART_DEBUG.println(address, HEX);
    }
  }
  if (nDevices == 0)
    RH_UART_DEBUG.println("No I2C devices found\n");
  else
    RH_UART_DEBUG.println("done\n");

  delay(3000);  // wait 5 seconds for next scan
}